# -*- coding: utf-8 -*-
"""
Solution to problem 1b of Assignment 2
@author: B Harish
"""
flag = 0
n = int(input('Enter the number of primes to display: '))
if n<=0:
    print('Invalid input. Enter a positive integer only')
else:
    if n==1:
        print('2')
    else:
        #Printing the first n primes
        print('2')
        inc = 0
        while(n>=2):
            x = 3+inc 
            #Checking if x is prime or not
            for i in range(2,(x//2)+1):
                if x%i == 0:
                    flag = 1
                    break
            if flag == 0:
                print(x)
                n-=1
            else:
                flag = 0
            inc+=2
            