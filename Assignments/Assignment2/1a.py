# -*- coding: utf-8 -*-
"""
Solution to problem 1a of Assignment 2
@author: B Harish
"""
flag = 0
x = int(input('Enter the number: '))
if x<=0:
    print('Invalid input. Enter a positive integer only')
else:
    if x==1:
        print(False)
    else:
         #Checking if the number is prime or not
         for i in range(2,(x//2)+1):
             if x%i == 0:
                 flag = 1
                 break
         if flag == 0:
             print(True)
         else:
             print(False)
