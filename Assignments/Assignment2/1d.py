# -*- coding: utf-8 -*-
"""
Solution to problem 1d of Assignment 2
@author: B Harish
"""
n = int(input('Enter the number to be checked: '))
if n<=1:
    print('Invalid input. Enter an integer greater than 2')
else:
    if n==2:
        print(False)
    elif n==6:
        print(True)
    else:
        #Perfect Numbers are always of the form (M*(M+1))/2, where M is the Mersenne Prime 2^p -1 and p is a prime number
        M=0
        num = 2*n
        i=1
        while i<(n+1)//2:
            if (i*(i+1))== num:
                M=i
                break
            i+=1
        if M==0:
            print(False)
        else:
            M+=1
            #Need to check if M is a power of 2. If yes, then the given number is a perfect number
            flag=0
            while(M!=1):
                if M%2==0:
                    M=M//2
                else:
                    flag=1
            if flag==0:
                print(True)
            else:
                print(False)
            