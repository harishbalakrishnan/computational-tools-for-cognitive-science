# -*- coding: utf-8 -*-
"""
Solution to problem 1e of Assignment 2
@author: B Harish
"""

n = int(input('Enter the number of perfect nos to be printed: '))
if n<=0:
    print('Invalid input. Enter a positive integer only')
elif n==1:
    print('6')
else:
    #Printing the first n perfect numbers
    print('6')
    #Euler-Euclid Theorem : Every even perfect number is of the form 2^p−1(2^p − 1) whenver 2^p-1 is prime
    #Also for it to be prime, it is necessary that p is prime
    p=3
    while(n>=2):
        #Checking if 2^p -1 is prime. If yes then we can generate a perfect number
        #The Mersenne Prime Mp
        Mp=(2**p)-1

        #Lucas-Lehmer Test to check if Mp is prime for odd p
        s = 4
        for i in range(p-2):
            s = (s*s-2)%Mp
        if s==0:
            print((Mp*(Mp+1))//2)
            n-=1

        #Incrementing p closer to the next prime. Seiving off odd numbers that are multiples of 3,5,7
        p+=2    
        if p%3==0:
            p+=2
        if p!=5 and p%5==0:
            p+=2
        if p!=7 and p%7==0:
            p+=2
        if p%3==0:
            p+=2
