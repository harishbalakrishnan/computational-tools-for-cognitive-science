# -*- coding: utf-8 -*-
"""
Solution to problem 1c of Assignment 2
@author: B Harish
"""
flag = 0
n = int(input('Enter the number of twin primes to display: '))
if n<=0:
    print('Invalid input. Enter a positive integer only')
else:
    if n==1:
        print('(3,5)')
    else:
        #Printing the first n twin primes
        print('(3,5)')
        inc = 0
        while(n>=2):
            x = 6+inc 
            #Checking prmality of x-1 and x+1
            for i in range(2,((x+2)//2)+1):
                if ((x-1)%i==0) or ((x+1)%i==0):
                    flag = 1
                    break
            if flag == 0:
                print('({},{})'.format(x-1,x+1))
                n-=1
            else:
                flag = 0
            inc+=6
            