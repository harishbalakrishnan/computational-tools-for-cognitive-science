#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Solution to problem 1f of Assignment 2
@author: B Harish
"""

#Getting date1 and date2 from the user
#Extracting dd,mm,yyyy from date_1
print('Enter the dd-mm-yyyy values one by one for date_1')
dd1=int(input('Enter dd : '))
mm1=int(input('Enter mm : '))
yyyy1=int(input('Enter yyyy : '))

#Extracting dd,mm,yyyy from date_2
print('\nEnter the dd-mm-yyyy values one by one for date_2')
dd2=int(input('Enter dd : '))
mm2=int(input('Enter mm : '))
yyyy2=int(input('Enter yyyy : '))


#Checking if the given dates are valid and proceeding accordingly
if not (dd1<=0 or dd2<=0 or dd1>31 or dd2>31 or mm1 <=0 or mm1>12 or mm2<=0 or mm2>12 or (dd1==31 and (mm1 in [9,4,6,11,2])) or (dd2==31 and (mm2 in [9,4,6,11,2])) or (dd1 ==30 and mm1 ==2) or (dd2==30 and mm2==2) or (dd1==29 and mm1==2 and ((yyyy1%4 !=0) or (yyyy1%4 ==0 and yyyy1%100==0 and yyyy1%400!=0))) or (dd2==29 and mm2==2 and ((yyyy2%4 !=0) or (yyyy2%4 ==0 and yyyy2%100==0 and yyyy2%400!=0)))):
	#Calculating absolute day number for Date1
	abs_date1=365*(yyyy1)
	leap_years = 0
	for i in range(0,yyyy1,4):
		leap_years+=1
	abs_date1+=leap_years
	#Counting the number of days passed in the current year
	#First counting the number of days passed upto the previous month
	for month in range(1,mm1):
		if month in [9,4,6,11]:
			abs_date1+=30
		elif month in [1,3,5,7,8,10]:
			abs_date1+=31
		elif month == 2 and ((yyyy1%4 !=0) or (yyyy1%4 ==0 and yyyy1%100==0 and yyyy1%400!=0)):
			abs_date1+=28
		elif month == 2 and (((yyyy1%4 ==0 and yyyy1%100!=0) or (yyyy1%4==0 and yyyy1%100==0 and yyyy1%400==0))):
			abs_date1+=29
	#Now counting the number of days passed in the current month
	i=1
	while(i<dd1):
		abs_date1+=1
		i+=1
	
	#Calculating absolute day number for Date2
	abs_date2=365*(yyyy2)
	leap_years = 0
	for i in range(0,yyyy2,4):
		leap_years+=1
	abs_date2+=leap_years
	#Counting the number of days passed in the current year
	#First counting the number of days passed upto the previous month
	for month in range(1,mm2):
		if month in [9,4,6,11]:
			abs_date2+=30
		elif month in [1,3,5,7,8,10]:
			abs_date2+=31
		elif month == 2 and ((yyyy2%4 !=0) or (yyyy2%4 ==0 and yyyy2%100==0 and yyyy2%400!=0)):
			abs_date2+=28
		elif month == 2 and (((yyyy2%4 ==0 and yyyy2%100!=0) or (yyyy2%4==0 and yyyy2%100==0 and yyyy2%400==0))):
			abs_date2+=29
	#Now counting the number of days passed in the current month
	i=1
	while(i<dd2):
		abs_date2+=1
		i+=1

	print('\n The number of days between the two dates : {}'.format(abs(abs_date1-abs_date2)))
else:
	print('Invalid Dates. Please provide valid dates')