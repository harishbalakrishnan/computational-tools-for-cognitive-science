"""
Solution to Problem 1a of Assignment 5
@author = B Harish
"""

# Stack class and related functions
class Stack():
	def __init__(self):
		self.objects = []

	def push(self,e):
		self.objects.append(e)

	def pop(self):
		n=len(self.objects)
		val = self.objects[n-1]
		self.objects.pop()
		return val

	def isEmpty(self):
		n = len(self.objects)
		if n == 0:
			return True
		else:
			return False

#Queue class and related functions
class Queue():
	def __init__(self):
		self.objects = []

	def isEmpty(self):
		if len(self.objects) == 0:
			return True
		else:
			return False

	def enqueue(self,e):
		self.objects.append(e)
	

	def dequeue(self):
		val = self.objects[0]
		self.objects.pop(0)
		return val



##### Test Cases for Stack
# S = Stack()
# S.push(6)
# print(S.isEmpty())
# print(S.pop())
# print(S.isEmpty())


#### Test Cases for Queue

# Q = Queue()
# print(Q.isEmpty())
# Q.enqueue(1)
# print(Q.isEmpty())
# Q.enqueue(8)
# print(Q.dequeue())
# print(Q.dequeue())
# print(Q.isEmpty())
