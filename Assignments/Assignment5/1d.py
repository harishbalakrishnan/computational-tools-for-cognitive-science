"""
Solution to Problem 1 of Assignment 5
@author = B Harish
"""


#Two data types Male and Female
class Male:
	def __init__(self,v=0):
		self.v = v

class Female:
	def __init__(self,v=0):
		self.v = v




# Stack class and related functions
class Stack:
	def __init__(self):
		self.objects = []

	def push(self,e):
		self.objects.append(e)

	def pop(self):
		n=len(self.objects)
		val = self.objects[n-1]
		self.objects.pop()
		return val

	def isEmpty(self):
		n = len(self.objects)
		if n == 0:
			return True
		else:
			return False

#Queue class and related functions
class Queue:
	def __init__(self):
		self.objects = []

	def isEmpty(self):
		if len(self.objects) == 0:
			return True
		else:
			return False

	def enqueue(self,e):
		self.objects.append(e)
	

	def dequeue(self):
		if self.isEmpty():
			return None
		else:
			val = self.objects[0]
			self.objects.pop(0)
			return val

#Indian Queue
class Indian_Queue:
	def __init__(self):
		self.Male_Q = Queue()
		self.Female_Q = Queue()

	def enqueue(self,e):
		if isinstance(e, Male):
			self.Male_Q.enqueue(e)
		else:
			self.Female_Q.enqueue(e)

	def dequeue(self):
		if self.Female_Q.isEmpty():
			if self.Male_Q.isEmpty():
				raise ValueError("Indian Queue is empty. Cannot Dequeue anymore")
			else:
				return self.Male_Q.dequeue().v
		else:
			return self.Female_Q.dequeue().v

	def isEmpty(self):
		if self.Female_Q.isEmpty() and self.Male_Q.isEmpty():
			return True
		else:
			return False


### Test cases
# S = Indian_Queue()
# m1 = Male(6)
# m2 = Male(7)
# f1 = Female(1)
# f2 = Female(2)
# S.enqueue(m1)
# S.enqueue(f1)
# S.enqueue(m2)
# S.enqueue(f2)
# print(S.dequeue())
# print(S.dequeue())
# print(S.dequeue())
# print(S.dequeue())


