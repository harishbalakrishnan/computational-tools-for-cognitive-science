"""
Solution to Problem 1 of Assignment 5
@author = B Harish
"""

# Stack class and related functions
class Stack():
	def __init__(self):
		self.objects = []

	def push(self,e):
		self.objects.append(e)

	def pop(self):
		n=len(self.objects)
		val = self.objects[n-1]
		self.objects.pop()
		return val

	def isEmpty(self):
		n = len(self.objects)
		if n == 0:
			return True
		else:
			return False

#Queue class and related functions
class Queue():
	def __init__(self):
		self.objects = []

	def isEmpty(self):
		if len(self.objects) == 0:
			return True
		else:
			return False

	def enqueue(self,e):
		self.objects.append(e)
	

	def dequeue(self):
		val = self.objects[0]
		self.objects.pop(0)
		return val

#Implementing A Queue class using Stack objects
class Queue_using_StackObjects():
	def __init__(self):
		self.S1 = Stack()
		self.S2 = Stack()

	def enqueue(self,e):
		self.S1.push(e)

	def dequeue(self):
		while(not(self.S1.isEmpty())):
			val = self.S1.pop()
			if (self.S1.isEmpty()):
				while(not(self.S2.isEmpty())):
					self.S1.push(self.S2.pop())
				return val
			else:
				self.S2.push(val)

	def isEmpty(self):
		if self.S1.isEmpty():
			return True
		else:
			return False


# Q = Queue_using_StackObjects()
# print(Q.isEmpty())
# Q.enqueue(6)
# print(Q.isEmpty())
# print(Q.dequeue())
# print(Q.isEmpty())
# Q.enqueue(7)
# Q.enqueue(8)
# Q.enqueue(9)
# print(Q.dequeue())
# print(Q.dequeue())
# print(Q.dequeue())
# print(Q.isEmpty())

