"""
Solution to Problem 2 of Assignment 5
@author = B Harish
"""

#Global Variables that will be used by functions
numbers = [str(x) for x in range(0,10)]+['_'+str(x) for x in range(1,10)]

#Defining priorities
priorities = {'(':3,'/':1,'*':1,'+':2,'-':2}

class Stack:
	def __init__(self):
		self.objects = []
		self.top = -1

	def push(self,e):
		self.objects.append(e)
		self.top+=1

	def pop(self):
		n=len(self.objects)
		if n == 0:
			raise ValueError("Stack Empty. Cannot pop anymore")
		else:
			val = self.objects[n-1]
			self.objects.pop()
			self.top-=1
			return val

	def isEmpty(self):
		n = len(self.objects)
		if n == 0:
			return True
		else:
			return False


def cleanup(expression):
	n=len(expression)
	i=0
	new_expression = []
	while(i<n):
		if expression[i]!='_':
			new_expression.append(expression[i])
			i+=1
		else:
			s='_'+expression[i+1]
			new_expression.append(s)
			i+=2
	return new_expression


def extract_operator(expression):
	# copy_expression = copy.deepcopy(expression)
	# copy_expression = list(filter(lambda x : x not in numbers,copy_expression))
	# copy_expression = list(filter(lambda x : x != "_", copy_expression))
	expression = cleanup(expression)
	postfix_expression = infix_to_postfix(expression)
	n=len(postfix_expression)
	return postfix_expression[n-1]

def infix_to_postfix(expression):
	#Cleaning up expression to account for negative integers
	expression = cleanup(expression)

	#Creating the stack
	s = Stack()

	postfix = []
	n = len(expression)
	expression = list(expression)
	i=0
	while(n>0):
		if i<len(expression) and expression[i] in numbers:
			postfix.append(expression[i])
			i+=1
			n-=1
		elif i<len(expression) and (expression[i] in priorities.keys() or expression[i] == '(' or expression[i] == ')'):
			if (s.isEmpty() or (expression[i] == '(') or (expression[i]!=')' and priorities[expression[i]]<priorities[s.objects[s.top]])):
				s.push(expression[i])
				i+=1
			elif expression[i] == ')':
				i+=1
				n-=1
				val = s.pop()
				while(val!='('):
					postfix.append(val)
					val = s.pop()
					n-=1
				else:
					n-=1
			else:
				val = s.pop()
				postfix.append(val)
				n-=1
		elif i >= len(expression):
			n=0
			while(not(s.isEmpty())):
				val = s.pop()
				postfix.append(val)
	postfix_expression = ''.join(postfix)
	return postfix_expression


def postfix_to_infix(expression):
	expression = cleanup(expression)
	infix = []
	s = Stack()
	n = len(expression)
	i=0
	while(i<n):
		if expression[i] in numbers:
			s.push(expression[i])
			i+=1
		else:
			current_operator = expression[i]
			
			#Extracting expression_2
			expression_2 = s.pop()
			n2 = len(expression_2)
			if n2>2:
				expression_2_operator = extract_operator(expression_2)
				if priorities[expression_2_operator] > priorities[current_operator]:
					expression_2 = '('+expression_2+')'

			#Extracting expression_1
			expression_1 = s.pop()
			n1 = len(expression_1)
			if n1>2:
				expression_1_operator = extract_operator(expression_1)
				if priorities[expression_1_operator] > priorities[current_operator]:
					expression_1 = '('+expression_1+')'

			string_to_push = expression_1+current_operator+expression_2
			s.push(string_to_push)
			i+=1
	return s.pop()



#Test Cases

# sample_string_1 = "2+_3*4"
# sample_string_2 = "(2+3)*4"
# sample_string_3 = "(_6+5)*(8+_9)"
# sample_string_4 = "2+(3+4)*5"

# sample_string_5 = infix_to_postfix(sample_string_3)
# sample_string_6 = infix_to_postfix(sample_string_1)
# sample_string_7 = infix_to_postfix(sample_string_2)

# print(postfix_to_infix(sample_string_5))
# print(postfix_to_infix(sample_string_6))
# print(postfix_to_infix(sample_string_7))
