"""
Solution to Problem 3 of Assignment 5
@author = B Harish
"""

#Global Variable
unbal = None
searched_node = None

#General purpose Functions
def inorder_traversal(tree_node):
	if tree_node==None:
		return None
	else:
		inorder_traversal(tree_node.leftchild)
		print(tree_node.val)
		inorder_traversal(tree_node.rightchild)

def postorder_traversal(tree_node):
	if tree_node==None:
		return None
	else:
		postorder_traversal(tree_node.leftchild)
		postorder_traversal(tree_node.rightchild)
		print(tree_node.val)

def max_depth(tree_node):
	if tree_node == None:
		return 0
	else:
		l=max_depth(tree_node.leftchild)
		r=max_depth(tree_node.rightchild)
		max_depth_value = max(l,r)+1
		return max_depth_value

def check_balanceFactor(tree_node):
	#Recursive Function to check for balance factor
	global unbal
	if tree_node == None:
		return None
	else:
		l= max_depth(tree_node.leftchild)
		r= max_depth(tree_node.rightchild)
		if tree_node.leftchild == None:
			balance_factor = r
		elif tree_node.rightchild == None:
			balance_factor = l
		else:
			balance_factor = l-r
		if abs(balance_factor)>1:
			unbal = tree_node
			return tree_node
		else:
			check_balanceFactor(tree_node.parent)

def ll_correction(parent_node,tree_node,child_node,bst_obj):
	if child_node.rightchild != None:
		tree_node.leftchild = child_node.rightchild
		child_node.rightchild.parent =  tree_node
	else:
		tree_node.leftchild = None

	child_node.rightchild = tree_node
	tree_node.parent = child_node

	#Retaining Earlier Connectivity
	if parent_node == None:
		bst_obj.root = child_node
	else:
		if parent_node.leftchild == tree_node:
			parent_node.leftchild = child_node
		else:
			parent_node.rightchild = child_node
	child_node.parent = parent_node

def rr_correction(parent_node,tree_node,child_node,bst_obj):
	if child_node.leftchild != None:
		tree_node.rightchild = child_node.leftchild
		child_node.leftchild.parent =  tree_node
	else:
		tree_node.rightchild = None

	child_node.leftchild = tree_node
	tree_node.parent = child_node

	#Retaining Earlier Connectivity
	if parent_node == None:
		bst_obj.root = child_node
	else:
		if parent_node.rightchild == tree_node:
			parent_node.rightchild = child_node
		else:
			parent_node.leftchild = child_node
	child_node.parent = parent_node

def lr_correction(tree_node,child_node,grandchild_node,bst_obj):
	if grandchild_node.leftchild!=None:
		child_node.rightchild = grandchild_node.leftchild
		grandchild_node.leftchild.parent = child_node
	else:
		child_node.rightchild = None

	child_node.parent=grandchild_node
	grandchild_node.leftchild = child_node
	grandchild_node.parent = tree_node
	tree_node.leftchild = grandchild_node
	ll_correction(tree_node.parent,tree_node,grandchild_node,bst_obj)

def rl_correction(tree_node,child_node,grandchild_node,bst_obj):
	if grandchild_node.rightchild!=None:
		child_node.leftchild = grandchild_node.rightchild
		grandchild_node.rightchild.parent = child_node
	else:
		child_node.leftchild = None

	child_node.parent=grandchild_node
	grandchild_node.rightchild = child_node
	grandchild_node.parent = tree_node
	tree_node.rightchild = grandchild_node
	rr_correction(tree_node.parent,tree_node,grandchild_node,bst_obj)

def balance_tree(tree_node,bst_obj):
	#Identifying the type of imbalance
	l = max_depth(tree_node.leftchild)
	r = max_depth(tree_node.rightchild)
	if l>r:
		child_node = tree_node.leftchild
		s1='l'
	else:
		child_node = tree_node.rightchild
		s1 = 'r'

	l = max_depth(child_node.leftchild)
	r = max_depth(child_node.rightchild)
	if l>r:
		grandchild_node = child_node.leftchild
		s2='l'
	else:
		grandchild_node = child_node.rightchild
		s2 = 'r'
	imbalance = s1+s2
	#Balancing it
	if imbalance == 'll':
		ll_correction(tree_node.parent,tree_node,child_node,bst_obj)
	elif imbalance == 'rr':
		rr_correction(tree_node.parent,tree_node,child_node,bst_obj)
	elif imbalance == 'lr':
		lr_correction(tree_node,child_node,grandchild_node,bst_obj)
	else:
		rl_correction(tree_node,child_node,grandchild_node,bst_obj)

def find_successor(tree_node):
	tree_node = tree_node.rightchild
	while(True):
		if tree_node.leftchild == None:
			return tree_node
		else:
			tree_node = tree_node.leftchild

def swap_nodes(tree_node_1_searched, tree_node_2_successor,bst_obj):
	global unbal
	unbal = None
	l= tree_node_1_searched.leftchild
	r = tree_node_1_searched.rightchild
	p = tree_node_1_searched.parent

	if tree_node_2_successor.parent.leftchild == tree_node_2_successor:
			tree_node_2_successor.parent.leftchild = None
	tree_node_2_successor.leftchild = tree_node_1_searched.leftchild
	if r != tree_node_2_successor:
		tree_node_2_successor.rightchild = r
	l.parent = tree_node_2_successor

	if r != tree_node_2_successor:
		r.parent = tree_node_2_successor
	if p == None:
		bst_obj.root = tree_node_2_successor
		tree_node_2_successor.parent = None
	else:
		#print(l.val,r.val,p.val)
		if p.leftchild == tree_node_1_searched:
			p.leftchild = tree_node_2_successor
		else:
			p.rightchild = tree_node_2_successor
		tree_node_2_successor.parent = tree_node_1_searched.parent

	unbal_node = check_balanceFactor(tree_node_2_successor.rightchild)
	if(unbal != None):
		balance_tree(unbal,bst_obj)
		nbal=None


#Class Definitions
class node():
	def __init__(self, val=0):
		self.val = val
		self.leftchild = None
		self.rightchild = None
		self.parent = None

class BST():
	def __init__(self):
		self.root = None
	
	def add(self,n):
		global unbal
		new_node = node(n)
		if self.root == None:
			self.root = new_node
		else:
			temp = self.root
			while(True):
				if new_node.val < temp.val and temp.leftchild != None:
					temp = temp.leftchild
				elif new_node.val < temp.val and temp.leftchild == None:
					temp.leftchild = new_node
					new_node.parent = temp
					unbal_node = check_balanceFactor(new_node.parent.parent)
					if(unbal != None):
					 	balance_tree(unbal,self)
					 	unbal=None
					break
				elif new_node.val > temp.val and temp.rightchild != None:
					temp = temp.rightchild
				else:
					temp.rightchild = new_node
					new_node.parent = temp
					unbal_node = check_balanceFactor(new_node.parent.parent)
					if unbal != None:
					 	balance_tree(unbal,self)
					 	unbal = None
					break

	def search(self,val):
		global searched_node
		searched_node = None
		if self.root == None:
			print('Tree in empty')
			return False
		else:
			temp = self.root
			while(True):
				if val == temp.val:
					searched_node = temp
					return True
				elif temp.leftchild != None and val<temp.val:
					temp = temp.leftchild
				elif temp.leftchild == None and val<temp.val:
					return False
				elif temp.rightchild != None and val>temp.val:
					temp = temp.rightchild
				elif temp.rightchild == None and val>temp.val:
					return False

	def delete(self,val):
		if self.search(val):
			# Case 1: The node to be deleted is a leaf node
			if searched_node.leftchild == None and searched_node.rightchild == None:
				if searched_node.parent.leftchild == searched_node:
					searched_node.parent.leftchild = None
				else:
					searched_node.parent.rightchild = None
			# Case 2: The node to be deleted has only left child
			elif searched_node.leftchild != None and searched_node.rightchild == None:
				if searched_node.parent.leftchild == searched_node:
					searched_node.parent.leftchild = searched_node.leftchild
					searched_node.leftchild.parent = searched_node.parent
				else:
					searched_node.parent.rightchild = searched_node.leftchild
					searched_node.leftchild.parent = searched_node.parent
			# Case 3: The node to be deleted has only right child
			elif searched_node.rightchild != None and searched_node.leftchild == None:
				if searched_node.parent.leftchild == searched_node:
					searched_node.parent.leftchild = searched_node.rightchild
					searched_node.rightchild.parent = searched_node.parent
				else:
					searched_node.parent.rightchild = searched_node.rightchild
					searched_node.rightchild.parent = searched_node.parent
			# Case 4: The node to be deleted has two children
			else:
				successor_node = find_successor(searched_node)
				swap_nodes(searched_node,successor_node,self)
		else:
			print('Value not present in tree')

	def isEmpty(self):
		if self.root == None:
			return True
		else:
			return False



#Testing
# obj = BST()

# obj.add(8)
# obj.add(12)
# obj.add(10)
# obj.add(15)
# obj.add(13)
# obj.add(5)
# obj.add(9)
# obj.add(14)
# obj.add(16)
# obj.delete(10)

# inorder_traversal(obj.root)
# postorder_traversal(obj.root)
