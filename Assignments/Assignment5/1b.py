"""
Solution to Problem 1 of Assignment 5
@author = B Harish
"""

# Stack class and related functions
class Stack():
	def __init__(self):
		self.objects = []

	def push(self,e):
		self.objects.append(e)

	def pop(self):
		n=len(self.objects)
		val = self.objects[n-1]
		self.objects.pop()
		return val

	def isEmpty(self):
		n = len(self.objects)
		if n == 0:
			return True
		else:
			return False

#Queue class and related functions
class Queue():
	def __init__(self):
		self.objects = []

	def isEmpty(self):
		if len(self.objects) == 0:
			return True
		else:
			return False

	def enqueue(self,e):
		self.objects.append(e)
	

	def dequeue(self):
		val = self.objects[0]
		self.objects.pop(0)
		return val

#Implementing a Stack using Queue objects
class Stack_using_QueueObjects():
	def __init__(self):
		self.Q1 = Queue()
		self.Q2 = Queue()

	def push(self,e):
		self.Q1.enqueue(e)

	def pop(self):
		while(not(self.Q1.isEmpty())):
			val = self.Q1.dequeue()
			if (self.Q1.isEmpty()):
				while(not(self.Q2.isEmpty())):
					self.Q1.enqueue(self.Q2.dequeue())
				return val
			else:
				self.Q2.enqueue(val)

	def isEmpty(self):
		if self.Q1.isEmpty():
			return True
		else:
			return False




# S = Stack_using_QueueObjects()
# print(S.isEmpty())
# S.push(6)
# print(S.isEmpty())
# print(S.pop())
# print(S.isEmpty())
# S.push(7)
# S.push(8)
# S.push(9)
# print(S.pop())
# print(S.pop())
# print(S.pop())
# print(S.isEmpty())

