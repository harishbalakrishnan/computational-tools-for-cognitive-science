import re

def month_string(mm):
	if mm == '01':
		month = 'Jan'
	elif mm == '02':
		month = 'Feb'
	elif mm == '03':
		month = 'Mar'
	elif mm =='04':
		month = 'Apr'
	elif mm == '05':
		month = 'May'
	elif mm == '06':
		month = 'Jun'
	elif mm == '07':
		month = 'Jul'
	elif mm == '08':
		month = 'Aug'
	elif mm == '09':
		month = 'Sep'
	elif mm == '10':
		month = 'Oct'
	elif mm == '11':
		month = 'Nov'
	elif mm == '12':
		month = 'Dec'
	else:
		raise ValueError('INVALID MONTH VALUE')
	return month

def count_newlines(s):
	n=len(s)
	count = 0
	for i in range(0,n):
		if s[i] =='\n':
			count+=1
	return count


rfd = open('3_testcases_2.txt') 
wfd = open('3_output.txt','w')
wfd.close()
wfd = open('3_output.txt','a')

output_date = []

file_string = rfd.read()
#Four formats possible
#Format 1 : dd-mm-yyyy to dd-mm-yyyy
match_expr_1 = r'\d\d-\d\d-\d\d\d\d\sto\s\d\d-\d\d-\d\d\d\d'
list1 = re.findall(match_expr_1,file_string)
for date in list1:
	from_month = month_string(date[:2])
	from_date = date[3:5]
	from_year = date[6:10]
	to_month = date[14:16]
	to_date = date[17:19]
	to_year = date[20:24]
	correct_string = from_date+'-'+from_month+'-'+from_year+' to '+to_date+'-'+to_month+'-'+to_year+('\n'*count_newlines(date))
	file_string = file_string.replace(date,correct_string)

#Format 2 : dd-mm-yyyy to dd-mm-yyyy
match_expr_2 = r'\d\d-\d\d-\d\d\d\d\sto\s\d\d-\d\d-\d\d\D'
list2 = re.findall(match_expr_2,file_string)
for date in list2:
	from_month = month_string(date[:2])
	from_date = date[3:5]
	from_year = date[6:10]
	to_date = date[14:16]
	to_month = date[17:19]
	to_year = '19'+date[20:22]
	correct_string = from_date+'-'+from_month+'-'+from_year+' to '+to_date+'-'+to_month+'-'+to_year+('\n'*count_newlines(date[:-1]))
	file_string = file_string.replace(date[:-1],correct_string)

#Format 3 : dd-mm-yyyy to dd-mm-yyyy
match_expr_3 = r'\d\d-\d\d-\d\d\sto\s\d\d-\d\d-\d\d\d\d'
list3 = re.findall(match_expr_3,file_string)
for date in list3:
	from_date = date[:2]
	from_month = month_string(date[3:5])
	from_year = '19'+date[6:10]
	to_month = date[12:14]
	to_date = date[15:17]
	to_year = date[18:22]
	correct_string = from_date+'-'+from_month+'-'+from_year+' to '+to_date+'-'+to_month+'-'+to_year+('\n'*count_newlines(date))
	file_string = file_string.replace(date,correct_string)

#Format 4 : dd-mm-yyyy to dd-mm-yyyy
match_expr_4 = r'\d\d-\d\d-\d\d\sto\s\d\d-\d\d-\d\d\D'
list4 = re.findall(match_expr_4,file_string)
for date in list4:
	from_date = date[:2]
	from_month = month_string(date[3:5])
	from_year = '19'+date[6:8]
	to_date = date[12:14]
	to_month = date[15:17]
	to_year = '19'+date[18:20]
	correct_string = from_date+'-'+from_month+'-'+from_year+' to '+to_date+'-'+to_month+'-'+to_year+('\n'*count_newlines(date[:-1]))
	file_string = file_string.replace(date[:-1],correct_string)

wfd.write(file_string)
rfd.close()
wfd.close()
