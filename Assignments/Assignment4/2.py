"""
Solution to Problem 2 of Assignment-4
@author: B Harish
"""

#Funtion to count the value of X
def count_x(string):
	count=0
	n=len(string)
	for i in range(0,n):
		if string[i] in first_half:
			count+=1

	return count

#Funtion to count the value of Y
def count_y(string):
	count=0
	n=len(string)
	for i in range(0,n):
		if string[i] in second_half:
			count+=1

	return count

#Generating the set X and Y
alphabet_upper=list(map(chr, range(65, 91)))
alphabet_lower=list(map(chr, range(97, 123)))
first_half = alphabet_lower[:13]+alphabet_upper[:13]
second_half = alphabet_lower[13:]+alphabet_upper[13:]

#Input and Output Files
rfd = open('2_testcases.txt') 
wfd = open('2_output.txt','w')
wfd.close()
wfd = open('2_output.txt','a')

#Main Logic
for lines in rfd.readlines():
	X = count_x(lines)
	Y = count_y(lines)
	lines=lines.strip('\n')
	if X-Y>1:
		output = lines + ' ' + 'Top heavy ' + str(X) + ' ' + str(Y) + '\n'
	elif Y-X>1:
		output = lines + ' ' + 'Bottom heavy ' + str(X) + ' ' + str(Y) + '\n'
	else:
		output = lines + ' ' + 'Balanced ' + str(X) + ' ' + str(Y) + '\n'
	
	#Writing the output on to the output file
	wfd.write(output)



#Closing the files
rfd.close()
wfd.close()
