"""
This is the solution to Problem 1 of Assignment 4
@author= B Harish
"""

import math

#Function that checks if the given value is a palindrome or not
def check_palindrome(val):
	str1=str(val)
	rev_str1=str1[::-1]
	if rev_str1==str1:
		return True
	else:
		return False

#Opening read and write files
rfd = open('1_testcases.txt')
wfd = open('1_solutions.txt','w')
wfd.close()
wfd = open('1_solutions.txt','a')

#Main Logic
i=0
for lines in rfd.readlines():
	line_array=[]
	n=len(lines)
	temp_string=''
	for i in range(0,n):
		if lines[i]==' ' or lines[i] =='\n':
			line_array.append(int(temp_string))
			temp_string=''
		else:
			temp_string+=lines[i]
	start_int=10**(line_array[1]-1)
	end_int = 10**(line_array[1])
	count=0
	palindromes=''
	for i in range(start_int,end_int):
		if(check_palindrome(i) and i%line_array[0]==0):
			count+=1
			palindromes = palindromes + str(i) + ', '
	a = str(line_array[0])
	b = str(line_array[1])
	line_1 = a+' '+b
	if count == 0:
		string=line_1+ '\n' + '0\n\n'
		wfd.write(string)
	else:
		palindromes=palindromes[:len(palindromes)-2]
		string=line_1+ '\n' + str(count) + '\n' + str(palindromes) + '\n'
		wfd.write(string)

#Closing the files
rfd.close()
wfd.close()