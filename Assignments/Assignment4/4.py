"""
Solution to Problem 4 of Assignment-4
@author: B Harish
"""

import re

def gcd(a,b):
    """a, b are +ve integers. Finds the greatest common divisor of a, b"""
    if a>0 and b>0: # the useful case
        while a!=0 and b!=0:
            if a>=b:
                a=a%b
            elif b>=a:
                b=b%a
        if a==0:
            gcd=b
        else:
            gcd=a
    else:
        gcd=None
    return gcd

def simplify(a,b,i,j,hcf):
	"""Divides the numerator and denominator by the GCD"""
	a[i] //=hcf
	b[j] //=hcf

def multiply(a):
	"""Multiplies all the elements of the list"""
	n=len(a)
	result=1
	for i in range(0,n):
		if a[i]!=1:
			result*=a[i]
	return result


#Input and Output Files
rfd = open('4_testcases_akshay.txt') 
wfd = open('4_output_akshay.txt','w')
wfd.close()
wfd = open('4_output_akshay.txt','a')

numerators = []
denominators = []

for line in rfd.readlines():
	if line=='\n':
		nr = multiply(numerators)
		dr = multiply(denominators)
		output_string = str(nr) + ',' + str(dr) + '\n'
		wfd.write(output_string)
		numerators = []
		denominators = []
	else:
		numerators_line = re.findall('\d+,',line)
		numerators_line = [int(i.strip(',')) for i in numerators_line]
		denominators_line = re.findall(',\d+',line)
		denominators_line = [int(i.strip(',')) for i in denominators_line]
		numerators+=numerators_line
		denominators+=denominators_line
		for i in range(0,len(numerators)):
			for j in range(0,len(denominators)):
				hcf = gcd(numerators[i],denominators[j])
				if hcf!=1:
					simplify(numerators,denominators,i,j,hcf)

rfd.close()
wfd.close()