"""
Solution to problem 4 of Assignment-3
@author: B Harish
"""

def num_steps(n):
	if n==1:
		return 1
	elif n==2:
		return 2
	elif n==3:
		return 4
	elif n>=4:
		return num_steps(n-1) + num_steps(n-2) + num_steps(n-3)


N=int(input('Value of N: '))
if N<=0:
	raise ValueError('INVALID Value. Enter a positive integer')
else:
	print('Number of possible ways to reach: '+str(num_steps(N)))
