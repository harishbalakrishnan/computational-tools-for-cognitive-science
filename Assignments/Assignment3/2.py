"""
Solution to Problem 1 of Assignment-3
@author: B Harish
"""

#Function that returns the character of the decription
def reverse_match(match_char,shift):
	alphabet=list(map(chr, range(65, 91))) 
	q_index=alphabet.index(match_char)
	if q_index-shift<0:
		return alphabet[26+q_index-shift]
	else:
		return alphabet[q_index-shift]

#Getting the coded text and the shift value from the user
coded_text=raw_input('CODED TEXT: ')
shift=int(input('SHIFT: '))

#Raising a value error if the shift value is not within limits
if not(shift<=26 and shift>=1):
	raise ValueError('INVALID SHIFT VALUE')

#Removing Spaces from the coded text
coded_text_without_spaces=''
n=len(coded_text)
for i in range(0,n):
	if coded_text[i]!=' ':
		coded_text_without_spaces+=coded_text[i]

#Decoding the text according to the shift value
decoded_text_with_space_character=''
n=len(coded_text_without_spaces)
for i in range(0,n):
	decoded_text_with_space_character+=reverse_match(coded_text_without_spaces[i],shift)

##Removing space characters and inserting space
decoded_text=''
n=len(decoded_text_with_space_character)
if n==2:
	decoded_text=" "
if n==3:
	decoded_text=decoded_text_with_space_character[0]
else:
	i=0
	while(i<n-2):
		if decoded_text_with_space_character[i]=='Q' and decoded_text_with_space_character[i+1]=='Q':
			decoded_text+=" "
			i+=2
		else:
			decoded_text+=decoded_text_with_space_character[i]
			i+=1

#Printing output
print('DECODED TEXT: '+decoded_text)

