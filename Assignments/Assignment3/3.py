#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 17 12:05:33 2018
@author: B Harish
"""

import queue
import copy
import math


#Defining a recursive function to encode the given string. This function returns the encrypted string
def encode(string):
    encoded_string=''
    q=queue.Queue()
    def inner_encode_function(string):
        nonlocal encoded_string
        nonlocal q
        n = len(string)
        if n == 1:
            encoded_string += string
        elif n > 1:
            # Splitting the parent string into left and right sub-strings
            if n % 2 == 0:
                encoded_string += string[(n // 2) - 1]
                l_string = string[:(n // 2) - 1]
                r_string = string[(n // 2):]
            else:
                encoded_string += string[(n // 2)]
                l_string = string[:(n // 2)]
                r_string = string[(n // 2) + 1:]
                # Adding the left and right child strings to the Queue
            q.put(l_string)
            q.put(r_string)
        if not (q.empty()):
            q_output = q.get()
            inner_encode_function(q_output)
        else:
            return
    inner_encode_function(string)
    return encoded_string

#Defining the decription function. This function returns the decrypted string
def decode(string):
    string=list(string)
    string_copy=copy.deepcopy(string)
    n=len(string)
    decoded_string=''
    for i in range(0,n):
        decoded_string+='-'
    decoded_string=list(decoded_string)
    free_indices=[x for x in range(0,n)]
    #Finding the depth of the tree-1
    d=int(math.log2(n))
    def inner_decode(string,l,r,i):
        if r==l and int(math.log2(i))<d:
            idx=(l+r)//2
            free_indices.remove(idx)
            decoded_string[idx]=string[i-1]   
            string_copy.remove(string[i-1])
            return
        elif r>l:
            idx=(l+r)//2
            free_indices.remove(idx)
            decoded_string[idx]=string[i-1]   
            string_copy.remove(string[i-1])
            i*=2
            inner_decode(string,l,idx-1,i)
            i+=1
            inner_decode(string,idx+1,r,i)
        else:
            return
    inner_decode(string,0,n-1,1)
    j=0
    for i in free_indices:
        decoded_string[i]=string_copy[j]
        j+=1
    return(''.join(decoded_string))
