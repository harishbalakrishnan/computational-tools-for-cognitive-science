"""
Solution to Problem 1 of Assignment-3
@author: B Harish
"""

#Global Variable that contains the currency notes used
notes=[0,0,0,0,0,0,0,0,0,0]
#Defining a recursive function to calculate the min number of notes/coins
def min_currency(n):
	if n>=2000:
		notes[9]+=1
		return (1+min_currency(n-2000))
	elif n>=500:
		notes[8]+=1
		return (1+min_currency(n-500))
	elif n>=200:
		notes[7]+=1
		return (1+min_currency(n-200))
	elif n>=100:
		notes[6]+=1
		return (1+min_currency(n-100))
	elif n>=50:
		notes[5]+=1
		return (1+min_currency(n-50))
	elif n>=20:
		notes[4]+=1
		return (1+min_currency(n-20))
	elif n>=10:
		notes[3]+=1
		return (1+min_currency(n-10))
	elif n>=5:
		notes[2]+=1
		return (1+min_currency(n-5))
	elif n>=2:
		notes[1]+=1
		return (1+min_currency(n-2))
	elif n>=1:
		notes[0]+=1
		return (1+min_currency(n-1))
	elif n==0:
		return 0

amount = int(input('Enter the amount: '))
if amount<0:
	raise ValueError('Invalid Amount. Enter a positive integer')
else:
	print('Minimum number of currency notes required: '+str(min_currency(amount)))
	if notes[0]!=0:
		print('Number of 1 rupee coins used: '+str(notes[0]))
	if notes[1]!=0:
		print('Number of 2 rupees= coins used: '+str(notes[1]))
	if notes[2]!=0:
		print('Number of 5 rupee denominations used: '+str(notes[2]))
	if notes[3]!=0:
		print('Number of 10 rupee denominations used: '+str(notes[3]))
	if notes[4]!=0:
		print('Number of 20 rupee denominations used: '+str(notes[4]))
	if notes[5]!=0:
		print('Number of 50 rupee denominations used: '+str(notes[5]))
	if notes[6]!=0:
		print('Number of 100 rupee denominations used: '+str(notes[6]))
	if notes[7]!=0:
		print('Number of 200 rupee denominations used: '+str(notes[7]))
	if notes[8]!=0:
		print('Number of 500 rupee denominations used: '+str(notes[8]))
	if notes[9]!=0:
		print('Number of 2000 rupee denominations used: '+str(notes[9]))


# Justification as why a greedy approach works here:
# The currency set {1,2,5,10,20,50,100,200,500,2000} forms a matroid. 
# Presence of a matroid structure here is a sufficient condtion for a greedy algorithm to work. 
# Hence it gurantees that the greedy solution will always yield the minimum possible number of currency notes required.
