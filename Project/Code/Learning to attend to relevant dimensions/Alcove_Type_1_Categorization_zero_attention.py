"""
Categorization Model: Alcove
@author: B Harish
"""

import math
import matplotlib.pyplot as plt 

# Creating Stimulus
stimulus = [(i,j,k) for i in range(0,2) for j in range(0,2) for k in range(0,2)]

# Initializing free parameters
c = 6.5
phi = 2.0
lambda_w = 0.03
lambda_alpha = 0

#Initializing Hyper parameters
r = 1
q = 1
alpha = [0.3333,0.3333,0.3333]
w = [[0,0] for i in range(8)]

#Initializing empty variables
a_hid = [0,0,0,0,0,0,0,0]
a_out = [0,0]
Pr = []

#Initializing Type 1 Categorization
type1_correct = [0,0,0,0,1,1,1,1]

type_1_correct_probabilities = []
type_1_alphas = [[],[],[]]

for cur in range(1000):
	# print('Parameters in Epoch {} : {},{}'.format(cur,w,alpha))
	if cur>=8:
		cur=cur%8
	current_stimulus = stimulus[cur]
	#Presenting a stimulus activates all the hidden nodes to different extents
	#Calculating Hidden layer node activations due to current stimulus
	for j in range(8):
		similarity_val = 0
		for i in range(3):
			similarity_val+=alpha[i]*abs(stimulus[j][i]-current_stimulus[i])
		a_hid[j]=math.exp(-c*similarity_val)

	# print('Activations : {}'.format(a_hid))

	#These activated hidden nodes inturn activates output nodes to different extents
	a_out = [0,0]
	for k in range(2):
		for j in range(8):
			a_out[k] += w[j][k]*a_hid[j]

	# print('Activations in output layer : {}'.format(a_out))

	#Calculating Pr[correct] values
	#For the first 4 stimuli, the correct category is 0 => Pr[0] needs to be calculated with a_out[0] and for the last 4 stimuli, the correct category is 1 => Pr[1] needs to be calculated with a_out[1]
	if cur<4:
		Pr.append(math.exp(phi*a_out[0])/(math.exp(phi*a_out[0])+math.exp(phi*a_out[1])))
	else:
		Pr.append(math.exp(phi*a_out[1])/(math.exp(phi*a_out[0])+math.exp(phi*a_out[1])))

	# print('Pr[correct] : {}'.format(Pr))


	##Learning

	#Initializing Teacher values
	if cur<4:
		tr = [max(1,a_out[0]),min(-1,a_out[1])]
	else:
		tr = [min(-1,a_out[0]),max(1,a_out[1])]
	
	# print('tr values : {}'.format(tr))

	#Error
	E = 0.5*(((tr[0]-a_out[0])**2)+((tr[1]-a_out[1])**2)) 

	# print('Error : {}'.format(E))

	# print('\n\n Del W values')
	#Online Learning of w
	for j in range(8):
		del_w = [lambda_w*(tr[0]-a_out[0])*a_hid[j],lambda_w*(tr[1]-a_out[1])*a_hid[j]]
		# print(del_w)
		w[j][0]+=del_w[0]
		w[j][1]+=del_w[1]

	# print('W updated : {}'.format(w))

	#Online Learning of alpha[i]
	del_alpha = [0,0,0]
	for i in range(3):
		outer_sum=0
		for j in range(8):
			#Calculating inner sigma for each hidden node
			sum_val = 0
			for k in range(2):
				sum_val+=(tr[k]-a_out[k])*w[j][k]

			outer_sum+=sum_val*a_hid[j]*c*abs(stimulus[j][i]-current_stimulus[i])
		del_alpha[i] = -lambda_alpha*outer_sum
		alpha[i]+=del_alpha[i]
		if alpha[i]<0:
			alpha[i] =0
		type_1_alphas[i].append([alpha[i]])

	# print('alpha updated : {}\n\n'.format(alpha))


# Writing Pr[Correct] values on to a file
wfd = open('Type_1_Pr_Zero.txt','w')
wfd.write(str(Pr))
wfd.close()



print('alpha updated : {}\n\n'.format(alpha))
print('weights updated : {}\n\n'.format(w))
#Plotting graph
plt.title('Type 1 Categorization') 
plt.plot(Pr)
plt.xlabel('Epoch') 
plt.ylabel('Pr[Correct]') 
plt.show()


plt.title('Shape Dimension Attention') 
plt.plot(type_1_alphas[0])
plt.xlabel('Epoch') 
plt.ylabel('Alpha') 
plt.show()


plt.title('Color Dimension Attention') 
plt.plot(type_1_alphas[1])
plt.xlabel('Epoch') 
plt.ylabel('Alpha') 
plt.show()


plt.title('Size Dimension Attention') 
plt.plot(type_1_alphas[2])
plt.xlabel('Epoch') 
plt.ylabel('Alpha') 
plt.show()



  
# function to show the plot 
plt.show()

		

