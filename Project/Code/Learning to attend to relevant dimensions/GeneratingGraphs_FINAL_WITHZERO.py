#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Categorization Model: Alcove
@author: thena
"""

import matplotlib.pyplot as plt


def avg_l(l):
    new_list = []
    for i in range(0,len(l)-8,8):
        avg_val = sum(l[i:i+8])/8
        new_list.append(avg_val)
    return new_list


#Getting Pr[Correct] values for Type_1 categorization

rfd = open('Type_1_Pr_Zero.txt')
Pr_1= rfd.read()
Pr_1 = Pr_1[1:len(Pr_1)-1]

Pr_1_list = Pr_1.split(',')
Pr_1_list = list(map(lambda x: x.replace(" ", ""),Pr_1_list))
Pr_1_list = list(map(lambda x: float(x),Pr_1_list))
Pr_1_list = avg_l(Pr_1_list)

rfd.close()


#Getting Pr[Correct] values for Type_2 categorization

rfd = open('Type_2_Pr_Zero.txt')
Pr_2= rfd.read()
Pr_2 = Pr_2[1:len(Pr_2)-1]

Pr_2_list = Pr_2.split(',')
Pr_2_list = list(map(lambda x: x.replace(" ", ""),Pr_2_list))
Pr_2_list = list(map(lambda x: float(x),Pr_2_list))
Pr_2_list = avg_l(Pr_2_list)

rfd.close()

#Getting Pr[Correct] values for Type_3 categorization

rfd = open('Type_3_Pr_Zero.txt')
Pr_3= rfd.read()
Pr_3 = Pr_3[1:len(Pr_3)-1]

Pr_3_list = Pr_3.split(',')
Pr_3_list = list(map(lambda x: x.replace(" ", ""),Pr_3_list))
Pr_3_list = list(map(lambda x: float(x),Pr_3_list))
Pr_3_list = avg_l(Pr_3_list)

rfd.close()

#Getting Pr[Correct] values for Type_4 categorization

rfd = open('Type_4_Pr_Zero.txt')
Pr_4= rfd.read()
Pr_4 = Pr_4[1:len(Pr_4)-1]

Pr_4_list = Pr_4.split(',')
Pr_4_list = list(map(lambda x: x.replace(" ", ""),Pr_4_list))
Pr_4_list = list(map(lambda x: float(x),Pr_4_list))
Pr_4_list = avg_l(Pr_4_list)

rfd.close()

#Getting Pr[Correct] values for Type_5 categorization

rfd = open('Type_5_Pr_Zero.txt')
Pr_5= rfd.read()
Pr_5 = Pr_5[1:len(Pr_5)-1]

Pr_5_list = Pr_5.split(',')
Pr_5_list = list(map(lambda x: x.replace(" ", ""),Pr_5_list))
Pr_5_list = list(map(lambda x: float(x),Pr_5_list))
Pr_5_list = avg_l(Pr_5_list)

rfd.close()

#Getting Pr[Correct] values for Type_6 categorization

rfd = open('Type_6_Pr_Zero.txt')
Pr_6= rfd.read()
Pr_6 = Pr_6[1:len(Pr_6)-1]

Pr_6_list = Pr_6.split(',')
Pr_6_list = list(map(lambda x: x.replace(" ", ""),Pr_6_list))
Pr_6_list = list(map(lambda x: float(x),Pr_6_list))
Pr_6_list = avg_l(Pr_6_list)

rfd.close()


plt.plot(Pr_1_list, 'r', label = 'Type I') # plotting t, a separately 
plt.plot(Pr_2_list, 'b', label = 'Type II') # plotting t, b separately 
plt.plot(Pr_3_list, 'g', label = 'Type III') # plotting t, c separately 
plt.plot(Pr_4_list, 'c', label = 'Type IV') 
plt.plot(Pr_5_list, 'm', label = 'Type V') 
plt.plot(Pr_6_list, 'y', label = 'Type VI') 

#plt.axis(0,50,0.5,1)
plt.xlim(0, 65)

plt.xlabel('Epoch')
plt.ylabel('Pr[Correct]')
plt.title('ALCOVE with zero attentional learning')
plt.legend()
plt.show()
