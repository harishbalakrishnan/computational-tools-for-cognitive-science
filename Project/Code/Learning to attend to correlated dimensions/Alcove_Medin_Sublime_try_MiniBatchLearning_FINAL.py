"""
Categorization Model: Alcove
@author: B Harish
"""

import math


def find_sum(del_w,i):
	n=len(del_w)
	sum_del = 0
	for j in range(0,n,8):
		sum_del+=del_w[i+j]
	return sum_del


# Creating Stimulus
stimulus = [(1,1,1,1),(0,1,1,1),(1,1,0,0),(1,0,0,0),(1,0,1,0),(0,0,1,0),(0,1,0,1),(0,0,0,1),(0,0,0,0),(0,0,1,1),(0,1,0,0),(1,0,1,1),(1,1,1,0),(1,1,0,1),(0,1,1,0),(1,0,0,1)]

# Initializing free parameters
c = 2.36
phi = 0.845
lambda_w = 0.026
lambda_alpha = 0.00965

#Initializing Hyper parameters
r = 1
q = 1
alpha = [0.25,0.25,0.25,0.25]
# print(alpha)
w = [[0,0] for i in range(8)]

#Initializing empty variables
a_hid = [0,0,0,0,0,0,0,0]
a_out = [0,0]
Pr = []
del_w = [[],[]]
del_alpha_batch = [[],[],[],[]]

#Initializing Type 1 Categorization
type1_correct = [0,0,0,0,1,1,1,1]

type_1_correct_probabilities = []
type_1_alphas = [[],[],[],[]]

for cur in range(400):
	# print('Parameters in Epoch {} : {},{}'.format(cur,w,alpha))
	if cur>=8:
		cur=cur%8
	current_stimulus = stimulus[cur]
	#Presenting a stimulus activates all the hidden nodes to different extents
	#Calculating Hidden layer node activations due to current stimulus
	for j in range(8):
		similarity_val = 0
		for i in range(4):
			similarity_val+=alpha[i]*abs(stimulus[j][i]-current_stimulus[i])
		a_hid[j]=math.exp(-c*similarity_val)


	#These activated hidden nodes inturn activates output nodes to different extents
	a_out = [0,0]
	for k in range(2):
		for j in range(8):
			a_out[k] += w[j][k]*a_hid[j]


	#Calculating Pr[] values
	
	Pr.append(math.exp(phi*a_out[0])/(math.exp(phi*a_out[0])+math.exp(phi*a_out[1])))

	## Learning

	#Initializing Teacher values
	if cur<4:
		tr = [max(1,a_out[0]),min(-1,a_out[1])]
	else:
		tr = [min(-1,a_out[0]),max(1,a_out[1])]
	

	#Error
	E = 0.5*(((tr[0]-a_out[0])**2)+((tr[1]-a_out[1])**2)) 

	#Mini-Batch Learning of w
	for j in range(8):
		del_w[0].append((tr[0]-a_out[0])*a_hid[j])
		del_w[1].append((tr[1]-a_out[1])*a_hid[j])

	if cur == 7:
		for j in range(8):
			w[j][0]+=lambda_w*find_sum(del_w[0],j)
			w[j][1]+=lambda_w*find_sum(del_w[1],j)
		del_w = [[],[]]


	#Mini-Batch Learning of alpha[i]
	del_alpha = [0,0,0,0]
	for i in range(4):
		outer_sum=0
		for j in range(8):
			#Calculating inner sigma for each hidden node
			sum_val = 0
			for k in range(2):
				sum_val+=(tr[k]-a_out[k])*w[j][k]

			outer_sum+=sum_val*a_hid[j]*c*abs(stimulus[j][i]-current_stimulus[i])
		del_alpha_batch[i].append(outer_sum)


	if cur == 7:
		for i in range(4):
			alpha[i]+=-lambda_alpha*sum(del_alpha_batch[i])
			if alpha[i]<0:
				alpha[i] =0
		del_alpha_batch = [[],[],[],[]]


# Writing Pr[Correct] values on to a file
wfd = open('Medin_Correlated.txt','w')
wfd.write(str(Pr))
wfd.close()

print('Updated Alpha : {}\n\n'.format(alpha))
print('Updated Weights : {}\n\n'.format(w))

# Printing P[Category = Terrigitis] Values for training stimulus
print("After Training:")
stimulus_names = ['T1','T2','T3','T4','M1','M2','M3','M4']
for i in range(8):
    print(stimulus_names[i]+': '+str(Pr[-(8-i)]))


# Finding Pr values for new stimulus 
Pr_new = []
new_stimulus = [(0,0,0,0),(0,0,1,1),(0,1,0,0),(1,0,1,1),(1,1,1,0),(1,1,0,1),(0,1,1,0),(1,0,0,1)]

for cur in range(8):
    current_stimulus = new_stimulus[cur]
    for j in range(8):
    	similarity_val = 0
    	for i in range(4):
    		similarity_val+=alpha[i]*abs(new_stimulus[j][i]-current_stimulus[i])
    	a_hid[j]=math.exp(-c*similarity_val)
    
    a_out = [0,0]
    for k in range(2):
    	for j in range(8):
    		a_out[k] += w[j][k]*a_hid[j]
    	
    Pr_new.append(math.exp(phi*a_out[0])/(math.exp(phi*a_out[0])+math.exp(phi*a_out[1])))

# Printing P[Category = Terrigitis] Values for new stimulus
print("\n\nFor test stimulus")
stimulus_names = ['N1','N2','N3','N4','N5','N6','N7','N8']
for i in range(8):
    print(stimulus_names[i]+': '+str(Pr_new[i]))
